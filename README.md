
Getting Started
-------
 - This module work for Oracle exp or imp.
 - Exporting is export data and upload dumpfile to S3.
   - config is expimp/expenv.var. Change some params for your env.
   - Initialize docker process is `script/export.sh` which work as start.sh on docker container.
   - Exporting process is `batch/run-exp.sh`

 - エクスポートでは、Oracleからdumpファイルを取得して、S３バケットへアップロードします。


 - Importing is initialize schema and import data from dumpfile.
   - config is expimp/impenv.var. Change some params for your env.
   - Initialize docker process is `script/import.sh` which work as start.sh on docker container.
   - Importing process is `batch/run-imp.sh`

- インポートでは、S3バケットからダウンロードして、Oracleへdumpファイルをインポートします。


Usage Export Data automatic
-------
 - make docker image and execute exp and upload S3
  - check batch/rundocker-exp.sh
  - this script is clone source and build dockerImage and run container. After complete, clean up work file and docker resources. 


Usage Import Data automatic
-------
 - make docker image and execute imp and download S3
  - check batch/rundocker-imp.sh


Building Docker Image
------

Export Image
```
cd workfolder
docker build -f Dockerfile.expjob -t testoraexp:latest .

```

Import Image
```
cd workfolder
docker build -f Dockerfile.impjob -t testoraexp:latest .

```


based:https://hub.docker.com/r/alexeiled/docker-oracle-xe-11g/
----------------------------------------------------------------
