<?php
require_once(__DIR__ . '/../aws/vendor/autoload.php');

use Aws\S3\S3Client;
use Aws\S3\MultipartUploader;
use Aws\Exception\MultipartUploadException;

$bucket = getenv('AWSBUCKET');
$keyname = getenv('S3EXPFILE');//'test5.zip';
//$source_file = fopen(__DIR__ . '/../output/tdm.zip', 'rb');
$source_file = '/data/exp.zip';

// $s3client = new Aws\S3\S3Client([
//     'credentials' => [
//         'key' => getenv('AWSACCESSKEY'),
//         'secret' => getenv('AWSSECRETKEY'),
//     ],
//     'region' => 'ap-northeast-1',
//     'version' => 'latest',
// ]);

$AWSACCESSKEY = getenv('AWSACCESSKEY');
$AWSSECRETKEY = getenv('AWSSECRETKEY');

$s3client = new Aws\S3\S3Client([
    'credentials' => [
        'key' => $AWSACCESSKEY,
        'secret' => $AWSSECRETKEY,
    ],
    'region' => 'ap-northeast-1',
    'version' => 'latest',
]);

$result = $s3client->putObject([
     'Bucket' => $bucket,
     'Key' => $keyname,
     'SourceFile' => $source_file,
     'ContentType' =>  'application/zip',
]);

//var_dump($result);
echo 0;
exit;

$source = $source_file;
$uploader = new MultipartUploader($s3client, $source, [
    'bucket' => $bucket,
    'key'    => $keyname,
]);

do {
    try {
        echo 'upload start.'."\n";
        $result = $uploader->upload();
    } catch (MultipartUploadException $e) {
        rewind($source);        
        echo $e->getMessage()."\n";
        sleep(5);
        $uploader = new MultipartUploader($s3client, $source, [
            'state' => $e->getState(),
        ]);
    }
} while (!isset($result));
echo 'upload complete.'."\n";
