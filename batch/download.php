<?php
require_once(__DIR__ . '/../aws/vendor/autoload.php');

use Aws\S3\S3Client;
use Aws\S3\MultipartUploader;
use Aws\Exception\MultipartUploadException;

$bucket = getenv('AWSBUCKET');
$keyname = getenv('S3EXPFILE');//'test5.zip';
//$source_file = fopen(__DIR__ . '/../output/tdm.zip', 'rb');
$source_file = '/data/imp/exp.zip';

// $s3client = new Aws\S3\S3Client([
//     'credentials' => [
//         'key' => getenv('AWSACCESSKEY'),
//         'secret' => getenv('AWSSECRETKEY'),
//     ],
//     'region' => 'ap-northeast-1',
//     'version' => 'latest',
// ]);

$AWSACCESSKEY = getenv('AWSACCESSKEY');
$AWSSECRETKEY = getenv('AWSSECRETKEY');

$s3client = new Aws\S3\S3Client([
    'credentials' => [
        'key' => $AWSACCESSKEY,
        'secret' => $AWSSECRETKEY,
    ],
    'region' => 'ap-northeast-1',
    'version' => 'latest',
]);

$result = $s3client->getObject([
     'Bucket' => $bucket,
     'Key' => $keyname,
     'SaveAs' => $source_file,
]);

//var_dump($result);
echo 0;
exit;