#!bin/bash
CPATH=`pwd`

php sendtopic.php "importing database starting download" "Import Database"

RESULT=`php download.php`

echo $RESULT;

if [ $RESULT = '0' ] ; then
    unzip /data/imp/exp.zip -d /data/imp

    mv /data/imp/data/exp.dmp /data/imp/exp.dmp
    
    sleep 5;
    php sendtopic.php "importing database starting import" "Import Database"

    imp $DBADMIN/$DBADMINPW@$DBHOST:$DBPORT/$DBSID FROMUSER=$FROMUSR TOUSER=$TOUSR parfile=/batchApp/expimp/imp.par

    php sendtopic.php "importing database successfully" "Import Database complete"

else
    #statements
    RESULT=`php sendtopic.php "download database dumpfile failed." "Import Database Fail"`
    exit -1;
fi
exit 0;