#!/bin/bash
IMAGE=mobaoraexp:letest

mkdir expwork

git clone https://hkd1123@bitbucket.org/hkd1123/docker-ora-expimp.git ./expwork

CPATH=`pwd`

cd $CPATH/expwork/

WRKPATH=`pwd`

echo 'building image'

CIID=`docker build -f Dockerfile.expjob -t $IMAGE .`

# echo $CIID
# exit;
##CMD="docker run -d --env-file $WRKPATH/expimp/expenv.var -v $WRKPATH/output:/data/ -v $WRKPATH/query:/query/ -e DBHOST=xxxxxxxx.ccgcdwvijwba.ap-northeast-1.rds.amazonaws.com $IMAGE"
CMD="docker run --env-file $WRKPATH/expimp/expenv.var -e DBADMIN=ACTDM -e DBHOST=xxxxx.ccgcdwvijwba.ap-northeast-1.rds.amazonaws.com -e AWSACCESSKEY=xxxx -e AWSSECRETKEY=xxxxxx -e AWSTOPIC=x -e AWSBUCKET=xxx -e S3EXPFILE=ssss -v $WRKPATH/output:/data/ -v $WRKPATH/query:/query/ $IMAGE"
CID=`$CMD`

echo 'running container'
echo $CID

sleep 10

echo 'stop container'
docker stop `docker ps -a -q`

sleep 30

echo 'delete container'
docker rm `docker ps -a -q`

sleep 20

echo 'delete image'
docker rmi `docker images -q mobaoraexp:letest`

sleep 30

cd $CPATH

rm -rf expwork

echo 'exporting complete'

# overwrite env
# docker run -d --env-file xxx -e EXPPASS=test 


