<?php
require_once(__DIR__ . '/../aws/vendor/autoload.php');

use Aws\S3\S3Client;
use Aws\S3\MultipartUploader;
use Aws\Exception\MultipartUploadException;
 
$bucket = getenv('AWSBUCKET');
$keyname = 'test3.zip';
$source_file = //fopen(__DIR__ . '/../output/tdm.zip', 'rb');
$source_file = __DIR__ . '/../output/tdm.zip';

$AWSACCESSKEY = getenv('AWSACCESSKEY');
$AWSSECRETKEY = getenv('AWSSECRETKEY');

$s3client = new Aws\S3\S3Client([
    'credentials' => [
        'key' => $AWSACCESSKEY,
        'secret' => $AWSSECRETKEY,
    ],
    'region' => 'ap-northeast-1',
    'version' => 'latest',
]);

$source = $source_file;
$uploader = new MultipartUploader($s3client, $source, [
    'bucket' => $bucket,
    'key'    => $keyname,
]);

try {
    echo 'upload start.'."\n";
    $promise = $uploader->promise();
    $promise->then(
        function ($value) {
            echo "The promise was fulfilled with {$value}"."\n";
            $result = true;
        },
        function ($reason) {
            echo "The promise was rejected with {$reason}"."\n";
            $result = false;
        }
    );
} catch (MultipartUploadException $e) {
    echo 'error';
    $result = false;

}

do {
    sleep(5);
    //$waitresult = $promise->wait();

    echo ".";

} while (!isset($result));
