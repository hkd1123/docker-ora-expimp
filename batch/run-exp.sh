#!bin/bash

CPATH=`pwd`

php sendtopic.php "exporting database starting exprt" "Export Database"

exp $DBADMIN/$DBADMINPW@$DBHOST:$DBPORT/$DBSID owner=$EXPUSR parfile=/batchApp/expimp/exp.par

sleep 5;

php sendtopic.php "exporting database starting upload" "Export Database"

zip /data/exp.zip /data/exp.dmp

sleep 5;

RESULT=`php upload.php`

echo $RESULT;

if [ $RESULT = "0" ] ; then
    php sendtopic.php "exporting database successfully" "Export Database complete"
else
    #statements
    php sendtopic.php "upload database dumpfile failed." "Export Database Fail"
    exit -1;
fi
exit 0;