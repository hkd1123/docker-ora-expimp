<?php
require_once(__DIR__ . '/../aws/vendor/autoload.php');

use Aws\Sns\SnsClient;

$msgstr = $argv[1];
$subject = $argv[2];

$AWSACCESSKEY = getenv('AWSACCESSKEY');
$AWSSECRETKEY = getenv('AWSSECRETKEY');
$AWSTOPIC = getenv('AWSTOPIC');

$snsclient = new SnsClient([
    'credentials' => [
        'key' => $AWSACCESSKEY,
        'secret' => $AWSSECRETKEY,
    ],
    'region' => 'ap-northeast-1',
    'version' => 'latest',
]);

$msg = [
    'Subject' => $subject,
    'Message' => $msgstr,
    'TargetArn' => $AWSTOPIC,
];

$result = $snsclient->publish($msg);