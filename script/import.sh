#!/bin/bash

set -e

export ORACLE_HOME=/u01/app/oracle/product/11.2.0/xe
export ORACLE_SID=XE
export PATH=$ORACLE_HOME/bin:$PATH

sed -i -E "s/HOST = [^)]+/HOST = ${HOSTNAME}/g" ${ORACLE_HOME}/network/admin/listener.ora
# service oracle-xe start

# echo "Database init..."
# for f in /etc/entrypoint-initdb.d/*; do
#   case "$f" in
#     *.sh)  echo "$0: running ${f}"; . "${f}" ;;
#     *.sql) echo "$0: running ${f}"; echo "@${f} ;" | sqlplus -S SYSTEM/oracle ;;
#     *)     echo "No volume sql script, ignoring ${f}" ;;
#   esac
#   echo
# done
# echo "End init."

echo "Oracle env setup successfully!"

# forever loop just to prevent Docker container to exit, when run as daemon
# while true; do sleep 1000; done
echo "Init aws sdk"

git clone $EXPIMPSRC /batchApp

echo "Start importing database"
cd /batchApp/batch

cd /data

if [ ! -d "/data/imp/" ]; then
  # Control will enter here if $DIRECTORY doesn't exist.
    mkdir imp
fi

cd /query

sqlplus $DBADMIN/$DBADMINPW@$DBHOST:$DBPORT/$DBSID @init.sql

cd /batchApp/batch

RESULT=`bash run-imp.sh`

